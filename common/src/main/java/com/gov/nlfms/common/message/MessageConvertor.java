package com.gov.nlfms.common.message;

public class MessageConvertor {
    public String convert(String string, String symbol) {
        StringBuilder stringBuilder = new StringBuilder();

        String message = "";

        for(int i=0 ; i<5; i++) {
            stringBuilder.append(symbol);
        }

        stringBuilder.append(" "+string+" ");

        for(int i=0 ; i<5; i++) {
            stringBuilder.append(symbol);
        }

        message = stringBuilder.toString();

        return message;
    }
}
