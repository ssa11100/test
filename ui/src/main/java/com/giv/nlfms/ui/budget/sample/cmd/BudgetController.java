package com.giv.nlfms.ui.budget.sample.cmd;

import com.gov.nlfms.budget.sample.svc.SampleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BudgetController {

    @GetMapping("/budget/sample")
    public String initView(){
        SampleService sampleService = new SampleService();
        String message = sampleService.sampleString("budget");

        return message;
    }
}
